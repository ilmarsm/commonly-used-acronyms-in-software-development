Commonly Used Acronyms in Software Development
-------------------------------------------------------------


A
AOP - Aspect Oriented Programming
AJAX - Asynchronous JavaScript And XML
AJP - Apache Java Protocol
ARP - Address Resolution Protocol



B
BDD - Behavior-Driven Development



C
CDN - Content Delivery Network
CLI - Command Line Interface
CMS - Content Management System
CRUD - Create, read, update, and delete
CSS - Cascading Style Sheets
CSV - Comma Separated Values



D
DCOM - Distributed Component Object Model
DHCP - Dynamic Host Configuration Protocol
DNS - Domain Name System protocol
DOM - Document Object Model



E
EOF - End of File
ETL - Extract, Transform, Load	


F
FIFO - First In First Out
FTP - File Transfer Protocol



G
GUI - Graphic User Interface



H
HTML - Hyper Text Markup Language
HTTP - Hyper Text Transfer Protocol
HQL - Hibernate Query Language


I
ICMP - Internet Control Message Protocol
IDE - Integrated Development Environment
IMAP4 - Internet Message Access Protocol (version 4)
IMAP - Internet Message Access Protocol


J
JDBC - Java Database Connectivity
JDK - Java Development Kit (contains JRE and JVM)
JMS - Java Message Service
JPA - Java Persistence API
JPQL - Java Persistence Query Language
JRE - Java Runtime Environment (only for runing/installing Java based applications - not for developing)
JS - JavaScript
JSON - JavaScript Object Notation
JVM - Java Virtual Machine (responsible for executing Java programs)



K



L
LPP - Lightweight Presentation Protocol



M
MVC - Model View Controller


N



O
OOP - Object Oriented Programming
ORM - Object to Relational Mapping


P
PHP - PHP: Hypertext Preprocessor
POJO Class - Plain Old Java Object Class
POP3 - Post Office Protocol (version 3)
POP - Post Office Protocol


R
REST - Representational State Transfer
RPC - Remote Procedure Call protocol


S
SAAS - Software as a Service
SDK - Software Development Kit
SEO - Search Engine Optimization
SMTP - Simple Mail Transfer Protocol
SNMP - Simple Network Management Protocol
SOAP - Simple Object Access Protocol
SOLID -  Single responsibility principle, Open-closed principle, Liskov substitution principle, Interface segregation principle, and Dependency inversion principle
SPA - Single Page  Application


T
TCP - Transmission Control Protocol
TDD - Teest-Driven-Development
Telnet - Terminal emulation protocol


U
UAT - User Acceptance Testing
UDP - User Datagram Protocol
UTC - Coordinated Universal Time
UX - User Experience



V



Z



X
XML - eXtensible Markup Language



Y
YAGNI -  You Ain’t Gonna Need It



Q

